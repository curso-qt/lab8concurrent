#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <QPromise>
#include <QFutureWatcher>
#include <QPromise>
#include <QtConcurrent/QtConcurrentRun>

void process(QPromise<void> &promise){
    promise.setProgressRange(0, 100);
    for (int i=0; i < 100; ++i) {
        promise.suspendIfRequested();

        if (promise.isCanceled()){
            return;
        }
        promise.setProgressValue(i);
        QThread::sleep(1);
    }
}

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    _watcher.setFuture(QtConcurrent::run(process));
    connect(&_watcher, &QFutureWatcher<void>::progressValueChanged, ui->progressBar, &QProgressBar::setValue);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btnPauseResume_clicked()
{
    if (ui->btnPauseResume->isChecked()){
        _watcher.future().suspend();
        ui->btnPauseResume->setText(QStringLiteral("Resume"));
    }else{
        _watcher.future().resume();
        ui->btnPauseResume->setText(QStringLiteral("Pause"));
    }
}

void MainWindow::on_btnCancelRestart_clicked()
{
    _watcher.future().cancel();
}
