#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QFuture>
#include <QFutureWatcher>
#include <QMainWindow>
#include <QPromise>
#include <QPushButton>


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_btnPauseResume_clicked();

    void on_btnCancelRestart_clicked();

private:
    Ui::MainWindow *ui;

    void definirTextoBotao(QPushButton *pushButton, const QString &texto);

    QFutureWatcher<void> _watcher;

};
#endif // MAINWINDOW_H
